/* Fill in your Name and GNumber in the following two comment fields
 * Name:
 * GNumber:
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "node.h"
#include "hash.h"

/* (IMPLEMENT THIS FUNCTION)
 * In this function, you will create a new Symtab struct.
 * Follow the Directions in the Project Documentation for this Function
 * Return the pointer to the new symtab.
 * On any memory errors, return NULL
 */
Symtab *hash_initialize() {
  Symtab *new = malloc(sizeof(Symtab));
  int i = 0;

  if (new == NULL) {
    return NULL; 
  }

  Symbol **arr = malloc(sizeof(Symbol*) * HASH_TABLE_INITIAL);

  for(i = 0; i < HASH_TABLE_INITIAL; i++){
    arr[i] = NULL;
  }

  new->size = 0;
  new->capacity = HASH_TABLE_INITIAL;
  new->table = arr;

  return new;
}

/* (IMPLEMENT THIS FUNCTION)
 * Destroy your Symbol Table.
 * Follow the Directions in the Project Documentation for this Function
 * Return on any memory errors.
 */
void hash_destroy(Symtab *symtab) {
  int i = 0;
  Symbol *itr = NULL;
  Symbol *reaper = NULL;
  
  if (symtab == NULL){
    return;
  }

  for(i = 0; i < symtab->size; i++){
    itr = symtab->table[i];
    while (itr != NULL) {
      reaper = itr;
      itr = itr->next;
      symbol_free(reaper);
    }
  }

  free(symtab->table);
  free(symtab);

  return;
}

/* (IMPLEMENT THIS FUNCTION)
 * Return the capacity of the table inside of symtab.
 * If symtab is NULL, return -1;
 */
int hash_get_capacity(Symtab *symtab) {
  if (symtab == NULL) {
    return -1;
  }
  return symtab->capacity;
}

/* (IMPLEMENT THIS FUNCTION)
 * Return the number of used indexes in the table (size) inside of symtab.
 * If symtab is NULL, return -1;
 */
int hash_get_size(Symtab *symtab) {
  if (symtab == NULL) {
    return -1;
  }
  return symtab->size;
}

/* (IMPLEMENT THIS FUNCTION)
 * Adds a new Symbol to the symtab via Hashing.
 * Follow the Directions in the Project Documentation for this Function
 * If symtab is NULL, there are any malloc errors, or if any rehash fails, return -1;
 * Otherwise, return 0;
 */
int hash_put(Symtab *symtab, char *var, int val) {
  long code = -1;
  int index = -1;
  Symbol *itr = NULL;
  Symbol *new = NULL;
  
  if (symtab == NULL) {
    return -1;
  }
  
  code = hash_code(var);
  index = code%(symtab->capacity);
  itr = symtab->table[index];

  if (itr != NULL) {
    while (itr->next != NULL) {
      if (strcmp(itr->variable, var) == 0) {
        itr->val = val;
        return 0;
      }

      itr = itr->next;
    }
  }

  if ((float)symtab->size/(float)symtab->capacity >= 2.0){
    hash_rehash(symtab, symtab->capacity * 2);
    index = code%(symtab->capacity);
  }

  itr = symtab->table[index];
  new = symbol_create(var, val);
  if (new == NULL) {
    return -1;
  }
  //check if new index has a Symbol list already
  if (itr != NULL) {
    while (itr->next != NULL){
      itr = itr->next;
    }
    itr->next = new;
  } else {
    symtab->table[index] = new;
  }

  symtab->size++;

  return 0;
}

/* (IMPLEMENT THIS FUNCTION)
 * Gets the Symbol for a variable in the Hash Table.
 * Follow the Directions in the Project Documentation for this Function
 * On any NULL symtab or memory errors, return NULL
 */
Symbol *hash_get(Symtab *symtab, char *var) {
  long code = -1;
  int index = -1;
  Symbol *itr = NULL;
  Symbol *copy = NULL;
  
  if (symtab == NULL) {
    return NULL;
  }
  
  code = hash_code(var);
  index = code%(symtab->capacity);
  itr = symtab->table[index];

  while (itr != NULL) {
    if (strcmp(itr->variable, var) == 0) {
      copy = symbol_copy(itr);
      break;
    }

    itr = itr->next;
  }

  return copy;
}

/* (IMPLEMENT THIS FUNCTION)
 * Doubles the size of the Array in symtab and rehashes.
 * Follow the Directions in the Project Documentation for this Function
 * If there were any memory errors, set symtab->array to NULL
 * If symtab is NULL, return immediately.
 */
void hash_rehash(Symtab *symtab, int new_capacity) {
  Symbol **temp = symtab->table;
  Symbol *itr = NULL;
  Symbol *hasher = NULL;
  int i;

  if (symtab == NULL) {
    return;
  }

  symtab->capacity = new_capacity;
  symtab->size = 0;
  symtab->table = malloc(sizeof(Symbol*) * new_capacity);
  if (symtab->table == NULL) {
    return;
  }

  for (i = 0; i < new_capacity; i++){
    symtab->table[i] = NULL;
  }

  for (i = 0; i < new_capacity/2; i++){
    if(temp[i] == NULL) {
      continue;
    }
    hasher = temp[i];
    itr = hasher->next;
    while(hasher != NULL) {

      hash_put(symtab, hasher->variable, hasher->val);
      symbol_free(hasher);

      hasher = itr;
      if (hasher != NULL) {
        itr = itr->next;
      }
    }
  }

  return;
}

/* Implemented for you.
 * Provided function to print the symbol table */
void hash_print_symtab(Symtab *symtab) {
  if(symtab == NULL) {
    return;
  }
  printf("|-----Symbol Table [%d size/%d cap]\n", symtab->size, symtab->capacity);

  int i = 0;
  Symbol *walker = NULL;

  /* Iterate every index, looking for symbols to print */
  for(i = 0; i < symtab->capacity; i++) {
    walker = symtab->table[i];
    /* For each found linked list, print every symbol therein */
    while(walker != NULL) {
      printf("| %10s: %d \n", walker->variable, walker->val);
      walker = walker->next;
    }
  }
  return;
}

/* This function is written for you.
 * This computes the hash function for a String
 */
long hash_code(char *var) {
  long code = 0;
  int i;
  int size = strlen(var);

  for(i = 0; i < size; i++) {
    code = (code + var[i]);
    if(size == 1 || i < (size - 1)) {
      code *= 128;
    }
  }

  return code;
}
